# VsCode仓库交互操作

+ 本地git命令

1. git init 初始化git仓库
2. git status 查看当前git的一个状态
3. git add 将未跟踪的文件跟踪，将纳入git管理的文件，将修改状态的文件添加到暂存区，可以将冲突文件标记已经解决
4. git commit  将放在暂存区的提交
5. git restore  取消对一个文件的修改
6. git diff 查看修改
7. git checkout 切换分支
8. git branch 新建分支
9. git merge 合并分支，常常要解决冲突
10. git stash 临时保存当前工作分支

+ 远程git命令

1. git clone
2. git pull
3. git push

**创建下仓库**

![1](img/1.png)

**点击加号来保存文件或者添加文件**

![2](img/2.png)

**提交之后就变成了A 进入暂存区域**

![3](img/3.png)

**输入内容提交**

![4](img/4.png)

**对修改的内容返回，前提是没有提交到暂存区**

![5](img/5.png)

**有需要的话可以不(add)点击那个加号，直接提交,属于智能提交**

![6](img/6.png)

**切换分支 git branch创建分支，check out切换分支，以当前分支为基础创建分支**

![7](img/7.png)

**输入分支名字，创建完成**

![8](img/8.png)

**解决冲突，在dev下面修改如下代码，点击提交**

![9](img/9.png)

**在master修改如下代码，点击齿轮，选择git merge ，选择dev分支比较**

**得到四个状态(采用当前分支，采用传入分支更改.....)   比较变更是看哪些变化，保留双方变更进入编辑状态，编辑更改之后，点击加号解决冲突**

**然后点击提交，提交代码。**

![10](img/10.png)

![11](img/11.png)

**git stash 存储   git stash pop应用存储**

![12](img/12.png)

**可以通过看边框颜色，看diff**

![13](img/13.png)

**git clone**

![14](img/14.png)

**Git push**   推送   push之前一般需要pull一下

![15](img/15.png)